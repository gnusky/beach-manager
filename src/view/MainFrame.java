package view;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1455759815358627822L;
	private static MainFrame instance;
	private final static int x = 1280;
	private final static int y = 800;
	private final JPanel backgroundPanel;
	private final JPanel initPanel = new JPanel();
	private final JLabel welcomeLabel = new JLabel("Benvenuto in Beach Manager!");
	
	private MainFrame(){
		this.setSize(new Dimension(MainFrame.x, MainFrame.y));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.backgroundPanel = new JPanel(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 7505904830766836252L;

			@Override
			  protected void paintComponent(Graphics g) {

			    super.paintComponent(g);
			        g.drawImage(new ImageIcon(this.getClass().getResource("/images/beachBackground.jpg")).getImage(), 0, 0, null);
			}
		};
		this.getContentPane().add(backgroundPanel);
		backgroundPanel.add(initPanel, BorderLayout.WEST);
		initPanel.setOpaque(false);
		initPanel.setLayout(new GridLayout(2,2));
		initPanel.add(Box.createRigidArea(new Dimension(15, 80)));
		welcomeLabel.setFont(new Font(Font.SANS_SERIF,Font.BOLD, 40));
		initPanel.add(welcomeLabel);
	}
	
	public static MainFrame getInstance() {
		if(instance == null) {
			instance = new MainFrame();
		}
		return instance;
	}
	
	
	
}
