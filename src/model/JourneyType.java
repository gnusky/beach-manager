package model;

public enum JourneyType {

	LOWDAY(1), 
	LOWWEEK(2), 
	MIDDAY(3), 
	MIDWEEK(4), 
	HIGHDAY(5), 
	HIGHWEEK(6), 
	JUNE(7), 
	JULY(8),
	AUGUST(9), 
	SEASON(10),
	CUSTOM(11);
	
	private final int number;
	
	private JourneyType(int number) {
		this.number = number;
	}
	
	public int getNumber() {
		return this.number;
	}
}
