package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class Journey {
	private final Beach beach;
	private final JourneyType type;
	private final List<Person> customers;
	private final Umbrella umbrella;
	// private final date
	private double toPay;
	private double totalAmount;
	private boolean isPayed;
	private Optional<List<Accessory>> accessories;

	public Journey(Beach beach, JourneyType type, List<Person> customers, Umbrella umbrella) {
		this.beach = beach;
		this.type = type;
		this.customers = new ArrayList<Person>(customers);
		this.umbrella = umbrella;
		this.toPay = calculatePrice();
		this.totalAmount = this.toPay;
		this.accessories = Optional.empty();
	}
	
	public List<Person> getCustomers() {
		return this.customers;
	}
	
	public double getAmountToPay() {
		return this.toPay;
	}

	public double getTotalAmount() {
		return this.totalAmount;
	}
	
	public Umbrella getUmbrella() {
		return this.umbrella;
	}
	
	public void setTotalAmount(double amount) {
		this.totalAmount = amount;
	}
	
	public void setAmountToPay(double amount) {
		this.toPay = amount;
	}
	
	public void pay(double amount) {
		this.toPay -= amount;
		if (this.toPay <= 0) {
			this.isPayed = true;
		}
	}

	public boolean hasBeenPayed() {
		return this.isPayed;
	}
	
	public void addAccessory(Accessory accessory) {
		this.accessories.get().add(accessory);
		this.totalAmount += accessory.getPrice(type);
	}

	private double calculatePrice() {
		Row myRow = beach.getRow(this.getUmbrella().getRowNumber());  // ricavo row da beach, metodo che itera
		if (type.equals(JourneyType.LOWDAY)) {					// le rows finch� non c'� il match con l'attuale
			return myRow.getLowDay();							// umbrella
		} else if (type.equals(JourneyType.LOWWEEK)) {
			return myRow.getLowWeek();
		} else if (type.equals(JourneyType.MIDDAY)) {
			return myRow.getMidDay();
		} else if (type.equals(JourneyType.MIDWEEK)) {
			return myRow.getMidWeek();
		} else if (type.equals(JourneyType.HIGHDAY)) {
			return myRow.getHighDay();
		} else if (type.equals(JourneyType.HIGHWEEK)) {
			return myRow.getHighWeek();
		} else if (type.equals(JourneyType.JUNE)) {
			return myRow.getJune();
		} else if (type.equals(JourneyType.JULY)) {
			return myRow.getJuly();
		} else if (type.equals(JourneyType.AUGUST)) {
			return myRow.getAugust();
		} else if (type.equals(JourneyType.SEASON)) {
			return myRow.getSeason();
		}
			//CUSTOM DA IMPLEMENTARE
		else return 0;
	}
}
