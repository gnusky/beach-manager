package model;

import java.util.ArrayList;
import java.util.List;

public class Beach {
	private final String name;
	private List<Row> rowList;

	public Beach(String name) {
		this.name = name;
		this.rowList = new ArrayList<Row>();
	}

	public String getName() {
		return this.name;
	}

	public List<Umbrella> getAllUmbrellas() {
		ArrayList<Umbrella> myList = new ArrayList<Umbrella>();
		for(Row r : this.rowList) {
			myList.addAll(r.getRowUmbrellas());
		}
		return myList;
	}
	
	public Row getRow(int index) {
		for(Row r : this.rowList) {
			if(r.getRowNumber() == index)
				return r;
		}
		return null;
	}

}
