package model;

public class Accessory {
	private final String name;
	private double dayPrice;
	private double weekPrice;
	private double monthPrice;
	private double seasonPrice;

	public Accessory(String name, double dayPrice, double weekPrice, double monthPrice, double seasonPrice){
		this.name = name;
		this.dayPrice = dayPrice;
		this.weekPrice = weekPrice;
		this.monthPrice = monthPrice;
		this.seasonPrice = seasonPrice;
	};
	
	public double getPrice(JourneyType type) {
		if(type == JourneyType.LOWDAY || type == JourneyType.MIDDAY || type == JourneyType.HIGHDAY) {
			return this.dayPrice;
		} else if (type == JourneyType.LOWWEEK || type == JourneyType.MIDWEEK || type == JourneyType.HIGHWEEK) {
			return this.weekPrice;
		} else if(type == JourneyType.JUNE || type == JourneyType.JULY || type == JourneyType.AUGUST) {
			return this.monthPrice;
		} else if(type == JourneyType.SEASON) {
			return this.seasonPrice;
		} else return setPrice();
			
	}
	////////DA IMPLEMENTARE IN VIEW
	private double setPrice() {
		return 0;
	}

}
