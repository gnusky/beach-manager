package model;

import java.util.Optional;

public class Umbrella {
	private final int number;
	private final int rowNumber;
	private boolean isBusy;
	private Optional<Journey> actual;

	public Umbrella(int number, int rowNumber) {
		this.number = number;
		this.rowNumber = rowNumber;
		this.isBusy = false;
		this.actual = Optional.empty();
	}

	public Umbrella getUmbrella() {
		return new Umbrella(this.getNumber(), this.getRowNumber());
	}

	public int getNumber() {
		return this.number;
	}

	public int getRowNumber() {
		return this.rowNumber;
	}

	public void addJourney(Journey journey) {
		this.actual = Optional.of(journey);
		this.isBusy = true;
	}

	public void deleteJourney() {
		this.actual = Optional.empty();
		this.isBusy = false;
	}

	public Optional<Journey> getOptJourney() {
		if (this.actual.isPresent()) {
			return this.actual;
		} else
			return Optional.empty();
	}
	
	public boolean getBusy() {
		return this.isBusy;
	}

}
