package model;

import java.util.ArrayList;
import java.util.List;

public class Row {
	private final int rowNumber;
	private double lowDayPrice;
	private double lowWeekPrice;
	private double midDayPrice;
	private double midWeekPrice;
	private double highDayPrice;
	private double highWeekPrice;
	private double junePrice;
	private double julyPrice;
	private double augustPrice;
	private double seasonPrice;
	private List<Umbrella> umbrellaList;

	public Row(int rowNumber) {
		this.rowNumber = rowNumber;
		this.umbrellaList = new ArrayList<Umbrella>();
	}

	// forse c'� anche da implementare il set single price per ogni coso, da vedere poi quando si implementa la gui
	public void setPrices(double LDP, double LWP, double MDP, double MWP, double HDP, double HWP, double june,
			double july, double aug, double season) {
		this.lowDayPrice = LDP;
		this.lowWeekPrice = LWP;
		this.midDayPrice = MDP;
		this.midWeekPrice = MWP;
		this.highDayPrice = HDP;
		this.highWeekPrice = HWP;
		this.junePrice = june;
		this.julyPrice = july;
		this.augustPrice = aug;
		this.seasonPrice = season;
	}

	public List<Umbrella> getRowUmbrellas() {
		return new ArrayList<Umbrella>(this.umbrellaList);
	}

	public void addUmbrella(Umbrella umbrella) {
		this.umbrellaList.add(umbrella);
	}

	public void deleteUmbrella(Umbrella umbrella) {
		this.umbrellaList.remove(umbrella);
	}

	public int getRowNumber() {
		return this.rowNumber;
	}

	public double getLowDay() {
		return this.lowDayPrice;
	}

	public double getLowWeek() {
		return this.lowWeekPrice;
	}

	public double getMidDay() {
		return this.midDayPrice;
	}

	public double getMidWeek() {
		return this.midWeekPrice;
	}

	public double getHighDay() {
		return this.highDayPrice;
	}

	public double getHighWeek() {
		return this.highWeekPrice;
	}

	public double getJune() {
		return this.junePrice;
	}

	public double getJuly() {
		return this.julyPrice;
	}

	public double getAugust() {
		return this.augustPrice;
	}

	public double getSeason() {
		return this.seasonPrice;
	}
}
