package model;

public class Person {
	private String name;
	private String surname;
	private String CF;

	public Person(String name, String surname, String CF) {
		this.name = name;
		this.surname = surname;
		this.CF = CF;
	}

	public Person getPerson() {
		return new Person(this.getName(), this.getSurname(), this.getCF());
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setCF(String CF) {
		this.CF = CF;
	}

	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public String getCF() {
		return this.CF;
	}

}
